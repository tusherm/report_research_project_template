# Scripts Predator-prey and Lorenz models

First open the file `plot_isocontours_pot_preypreda.py`, study it and try to
execute it. From this script, you can learn how to save a figure in a
particular directory.

Then, do the same for the files `job_sim_predaprey.py` and `job_sim_lorenz.py`.
For these file you should use IPython and try to plot other quantities.

Note that after that you used a function that created a figure, you can create
variables pointing towards the corresponding axis and figure with the following
code:

```python
import matplotlib.pyplot as plt

ax = plt.gca()
fig = ax.figure
```

Then, try to write some scripts to plot and save figures (save the scripts in
you own repository!). When you are happy with a figure, keep the corresponding
script as it is. Use another script for another figure. Like that it's very
easy to recreate your figure (potentially slightly modified).
